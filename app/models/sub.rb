class Sub < ActiveRecord::Base
  validates :title, :description, :user_id, presence: true
  validates :title, uniqueness: true

  belongs_to(
    :moderator,
    class_name: 'User',
    primary_key: :id,
    foreign_key: :user_id
  )

end
