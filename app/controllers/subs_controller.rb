class SubsController < ApplicationController

  def index
    render :index
  end

  def new
    if logged_in?
      @sub = Sub.new
      render :new
    else
      escape
    end
  end

  def create
    @sub = Sub.new(sub_params)
    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash[:errors] = ["Not a valid subrequest."]
      render :new
    end
  end

  def show
    @sub = Sub.find(params[:id])
  end

  private

  def sub_params
    params.require(:sub).permit :title, :description, :user_id
  end

  def escape
    redirect_to new_session_url
  end

end
