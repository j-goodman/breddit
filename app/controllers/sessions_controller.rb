class SessionsController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.find_by_credentials(user_params[:name], user_params[:password])
    login_user!
  end

  def destroy
    logout_user!
  end

end
