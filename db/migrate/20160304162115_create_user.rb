class CreateUser < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :password_digest, null: false
      t.string :session_token

      t.timestamps
    end
    add_index :users, :name, unique: :name
  end
end
