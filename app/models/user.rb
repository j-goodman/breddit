class User < ActiveRecord::Base
  validates :name, :password_digest, presence: true
  validates :name, uniqueness: true
  validates :password, length: { minimum: 6 }, allow_nil: true
  after_initialize :ensure_session_token
  attr_reader :password

  has_many(
    :subs,
    class_name: 'Sub',
    primary_key: :id,
    foreign_key: :user_id
  )

  def password=(pword)
    self.password_digest = BCrypt::Password.create(pword)
  end

  def is_password?(pword)
    BCrypt::Password.new(password_digest).is_password?(pword)
  end

  def self.find_by_credentials(name, pword)
    user = User.find_by(name: name)
    return user if user && user.is_password?(pword)
    nil
  end

  def reset_session_token!
    self.session_token = generate_unique_token
    self.save!
  end

  def ensure_session_token
    self.session_token ||= generate_unique_token
  end

  private


  def generate_unique_token
    new_token = SecureRandom.base64
    while User.exists?(session_token: new_token)
      new_token = SecureRandom.base64
    end
    new_token
  end

end
