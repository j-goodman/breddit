Rails.application.routes.draw do
  resources :users, only: [:new, :show, :create]
  resources :subs, except: [:destroy]
  resource :session, only: [:new, :create, :destroy]
end
