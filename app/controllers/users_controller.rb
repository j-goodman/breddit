class UsersController < ApplicationController

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login_user!
      redirect_to subs_url
    else
      flash[:errors] = ["Invalid user input"]
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    render :show
  end

end
